﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace TaskChaining
{
    public class Program
    {
        public int[] GenerateArray(int[] arr)
        {
            Random random = new Random();

            if (arr.Any(x => x <= 0))
            {
                throw new ArgumentException(nameof(arr));
            }

            Task<int[]> task1 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    arr[i] = random.Next(1, 100);
                }
                return arr;
            });

            return task1.Result;
        }

        public int[] MultiplyTheArray(int[] arr, int[] secArr)
        {
            if (arr.Any(x => x <= 0))
            {
                throw new ArgumentException(nameof(arr));
            }

            if (secArr.Any(x => x <= 0))
            {
                throw new ArgumentException(nameof(arr));
            }

            Task<int[]> task2 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    arr[i] *= secArr[i];
                }

                return arr;
            });

            return task2.Result;
        }

        public int[] SortArray(int[] arr)
        {
            if (arr.Any(x => x <= 0))
            {
                throw new ArgumentException(nameof(arr));
            }

            Task<int[]> task3 = Task.Run(() =>
            {
                Array.Sort(arr);

                return arr;
            });

            return task3.Result;
        }

        public float Average(int[] arr)
        {
            Task<float> task4 = Task.Run(() =>
            {
                float sum = 0;
                float avg = 0;
                int size = arr.Length;
                for (int i = 0; i < size; i++)
                {
                    sum += arr[i];
                }

                avg = sum / size;

                return avg;
            });

            return task4.Result;
        }

        static void Main(string[] args)
        {
            //I leave this code in main method to showcase continuation with tasks.
            Random random = new Random();

            Task<int[]> task1 = Task.Run(() =>
            {
                int[] arr = new int[10];

                for (int i = 0; i < 10; i++)
                {
                    arr[i] = random.Next(1, 100);
                }
                return arr;
            });

            Console.Write("Task1 => array: { ");
            foreach (int a in task1.Result)
            {
                Console.Write(a + ", ");
            }
            Console.Write("}");

            Task<int[]> task2 = task1.ContinueWith(x =>
            {

                int[] arr = new int[10];

                for (int i = 0; i < 10; i++)
                {
                    arr[i] = task1.Result[i] * random.Next(1, 100);
                }

                return arr;
            });

            Console.WriteLine();

            Console.Write("Task2 => array: { ");
            foreach (int b in task2.Result)
            {
                Console.Write(b + ", ");
            }
            Console.Write("}");

            Task<int[]> task3 = task1.ContinueWith(x =>
            {
                int[] arr = task1.Result;

                Array.Sort(arr);

                return arr;
            });

            Console.WriteLine();

            Console.Write("Task3 => array: { ");
            foreach (int c in task3.Result)
            {
                Console.Write(c + ", ");
            }
            Console.Write("}");

            Task<float> task4 = task1.ContinueWith(x =>
            {
                int[] arr = task1.Result;
                float sum = 0;
                float avg = 0;
                int size = arr.Length;
                for (int i = 0; i < size; i++)
                {
                    sum += arr[i];
                }

                avg = sum / size;

                return avg;
            });

            Console.WriteLine();

            Console.Write("Task4 => average of array: " + task4.Result);

        }
    }
}
