using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TaskChaining;

namespace TaskChainingTests
{
    [TestFixture]
    public class TaskChainingTests
    {
        readonly Program p = new Program();

        [TestCase(new int[] { 5, 4, 3, 2, 1, 10, 9, 8, 7, 6 })]

        public void GenerateArrayTestSuccess(int[] arr)
        {
            var result = p.GenerateArray(arr);

            Assert.AreEqual(arr, result);
        }

        //if array contains 0 it must return argument null exception
        [TestCase(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 })]
        [TestCase(new int[] { 5, 4, 3, 2, 1, 0, 9, 8, 7, 6 })]

        public void GenerateArrayTestFailure(int[] arr)
        {
            Assert.Throws<ArgumentException>(() => p.GenerateArray(arr));
        }

        [TestCase(new int[] { 3, 2, 5, 4, 1, 9, 10, 8, 7, 6 })]
        public void MultipleArraySucces(int[] arr)
        {
            Random random = new Random();
            int[] newArr = new int[10];

            for (int i = 0; i < 10; i++)
            {
                newArr[i] = random.Next(1, 100);
            }

            var result = p.MultiplyTheArray(arr, newArr);

            for (int i = 0; i < 10; i++)
            {
                arr[i] *= newArr[i];
            }

            Assert.AreEqual(arr, result);
        }

        //if any of the given arrays contain zero or lower then it must return exception
        [TestCase(new int[] { 5, 4, 3, 2, 1, 0, 9, 8, 7, 6 }, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        [TestCase(new int[] { 5, 4, 3, 2, 1, 10, 9, 8, 7, 6 }, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 })]
        public void MultiplyArrayFailure(int[] arr, int[] secArray)
        {
            Assert.Throws<ArgumentException>(() => p.MultiplyTheArray(arr, secArray));

        }

        [TestCase(new int[] { 5, 4, 3, 2, 1, 10, 9, 8, 7, 6 })]
        [TestCase(new int[] { 10, 9, 3, 2, 1, 5, 4, 8, 7, 6 })]
        public void SortArraySuccess(int[] arr)
        {
            var result = p.SortArray(arr);

            Array.Sort(arr);

            Assert.AreEqual(arr, result);
        }

        [TestCase(new int[] { 5, 4, 3, 2, 1, 0, 9, 8, 7, 6 })]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 })]

        public void SortArrayFailure(int[] arr)
        {
            Assert.Throws<ArgumentException>(() => p.SortArray(arr));
        }

        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 15 }, ExpectedResult = 6f)]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 19, 15 }, ExpectedResult = 7f)]

        public float GetAvgSucces(int[] arr)
        {
            var result = p.Average(arr);

            return result;
        }
    }
}
